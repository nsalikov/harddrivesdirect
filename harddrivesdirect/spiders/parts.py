# -*- coding: utf-8 -*-
import re
import scrapy
from urllib.parse import urlparse, urlunparse, parse_qsl, urlencode


class PartsSpider(scrapy.Spider):
    name = 'parts'
    allowed_domains = ['harddrivesdirect.com']
    start_urls = [
        'https://www.harddrivesdirect.com/index.php?cPath=20',
        'https://www.harddrivesdirect.com/index.php?cPath=40',
        'https://www.harddrivesdirect.com/index.php?cPath=60',
        'https://www.harddrivesdirect.com/index.php?cPath=70',
        'https://www.harddrivesdirect.com/index.php?cPath=87',
        'https://www.harddrivesdirect.com/index.php?cPath=99',
        'https://www.harddrivesdirect.com/index.php?cPath=100',
        'https://www.harddrivesdirect.com/index.php?cPath=101',
        'https://www.harddrivesdirect.com/index.php?cPath=102',
        'https://www.harddrivesdirect.com/index.php?cPath=113',
        'https://www.harddrivesdirect.com/index.php?cPath=124',
        'https://www.harddrivesdirect.com/index.php?cPath=125',
        'https://www.harddrivesdirect.com/index.php?cPath=126',
        'https://www.harddrivesdirect.com/index.php?cPath=129',
        'https://www.harddrivesdirect.com/index.php?cPath=130',
        'https://www.harddrivesdirect.com/index.php?cPath=131',
        'https://www.harddrivesdirect.com/index.php?cPath=150',
        'https://www.harddrivesdirect.com/index.php?cPath=200',
        'https://www.harddrivesdirect.com/index.php?manufacturers_id=5',
        'https://www.harddrivesdirect.com/index.php?manufacturers_id=10',
        'https://www.harddrivesdirect.com/index.php?manufacturers_id=11',
        'https://www.harddrivesdirect.com/index.php?manufacturers_id=12',
        'https://www.harddrivesdirect.com/index.php?manufacturers_id=13',
        'https://www.harddrivesdirect.com/index.php?manufacturers_id=14',
        'https://www.harddrivesdirect.com/index.php?manufacturers_id=16',
        'https://www.harddrivesdirect.com/index.php?manufacturers_id=22',
        'https://www.harddrivesdirect.com/index.php?manufacturers_id=36',
    ]


    def parse(self, response):
        items_css = 'table tr.rounded_new2 td.rounded_new a ::attr(href)'
        items = response.css(items_css).extract()

        for item in normalize_items(items):
            yield response.follow(item, callback=self.parse_item)

        next_page_xpath = "//td[contains(@class, 'roboto_black13')]/a[contains(@title, 'Next Page')]/@href"
        next_page = response.xpath(next_page_xpath).extract_first()

        if next_page:
            yield response.follow(normalize_next_page(next_page), callback=self.parse)


    def parse_item(self, response):
        d = {}

        d['name'] = _text(response.css('h2 ::text').extract())
        d['url'] = response.request.url
        d['id'] = _text(response.css('[itemprop="productID"] ::text').extract_first())
        d['mpn'] = _text(response.css('[itemprop="mpn"] ::text').extract_first())
        d['category'] = _text(response.css('[itemprop="category"] ::text').extract())
        d['inventory'] = _text(response.xpath('//*[@itemprop="inventoryLevel"]/text()').extract())
        d['warranty'] = _text(response.css('[itemprop="warranty"] ::text').extract_first())
        d['condition'] = _text(response.css('[itemprop="itemCondition"] ::text').extract_first())
        d['price'] = _text(response.css('.product_info_usa .roboto_red14 ::text').extract_first())

        d['description'] = list(filter(None, [t.strip(':').strip() for t in response.css('[itemprop="description"] ::text').extract()]))
        d['overview'] = _text(response.xpath('//*[contains(@class, "roboto_black11")]/b[contains(text(), "Overview:")]/ancestor::p[1]/descendant-or-self::*/text()').extract())
        d['overview'] = re.sub('^Overview:\s*', '', d['overview'])

        d['specs'] = {}

        trs = response.xpath('//*[contains(@class, "roboto_black11")]/b[contains(text(), "Specifications:")]/ancestor::table[1]/child::tr')

        for tr in trs:
            tds = tr.css('tr>td ::text').extract()

            if len(tds) == 2:
                key, value = [td.strip() for td in tds]
                d['specs'][key] = value

        d['compatible'] = list(filter(None, [t.replace('\xa0', ' ').strip() for t in response.css('[itemprop="isAccessoryOrSparePartFor"] ::text').extract()]))

        # for line in d['compatible']:
        #     chunks = line.split(':')
        #     if len(chunks) == 2:
        #         key, value = chunks
        #         key = key.strip()
        #         value = re.findall('([\S]+\s*\(.*?\)|[\S]+)', value.strip())
        #         d['compatible_parsed'][key] = value

        yield d


def normalize_items(urls):
    product_url = 'https://www.harddrivesdirect.com/product_info.php?products_id={}'

    for u in urls:
        match = re.search('products_id=(\d+)', u)
        if match:
            yield product_url.format(match.group(1))


def normalize_next_page(next_page):
    params_to_delete = ['PHPSESSID', 'currency', 'sort']

    p = list(urlparse(next_page))
    q = dict(parse_qsl(p[4]))

    try:
        for param in params_to_delete:
            if param in q:
                del q[param]

        p[4] = urlencode(q)
        next_page = urlunparse(p)
    except:
        pass

    return next_page


def _text(text):
    if text is None:
        return ''

    if type(text) == str:
        text = [text]

    text = ' '.join(filter(None, [t.replace('&nbsp;', ' ').replace('\xa0', ' ').strip() for t in text if t is not None]))

    return text
