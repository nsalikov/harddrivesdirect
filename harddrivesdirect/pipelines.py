# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html

from scrapy.exceptions import DropItem


class DuplicatesPipeline(object):

    def __init__(self):
        self.seen = set()


    def process_item(self, item, spider):
        keys = ['id']

        return self.filter_by_keys(item, spider, keys)


    def filter_by_keys(self, item, spider, keys):
        values = [item[key] for key in keys if key in item]
        s = ', '.join(values)

        if s in self.seen:
            raise DropItem("Duplicate. {}:".format(s))

        self.seen.add(s)

        return item